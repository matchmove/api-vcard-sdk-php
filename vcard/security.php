<?php
namespace MatchMovePay\VCard;

class Security {
    protected $cipher    = MCRYPT_RIJNDAEL_128;
    protected $mode      = MCRYPT_MODE_CBC;
    protected $key       = null;
    protected $iv_size   = null;
    protected $random    = MCRYPT_RAND;
    protected $salt_size = 8;
    
    public function __construct($key, $cipher = MCRYPT_RIJNDAEL_128, $mode = MCRYPT_MODE_CBC) {
        $this->cipher = $cipher;
        $this->mode   = $mode;
        $this->key   = $key;

        $iv_size = phpseclib_mcrypt_get_iv_size($this->cipher, $this->mode);

		if (strlen($this->key) > ($key_max_len = phpseclib_mcrypt_get_key_size($this->cipher, $this->mode))) {
			throw new \InvalidArgumentException(strtr(
                'The key length must be less or equal than :max_len.',
                [':max_len' => $key_max_len]));
		}
        
        $this->iv_size = $iv_size;
    }

    
    public function encrypt($data) {
    
        // Set a random salt
        $salt = substr(md5(mt_rand(), true), $this->salt_size);
        
        $block = phpseclib_mcrypt_get_block_size($this->cipher, $this->mode);
        $pad = $block - (strlen($data) % $block);
    
        $data = $data . str_repeat(chr($pad), $pad);
    
        // Setup encryption parameters
        $td = phpseclib_mcrypt_module_open($this->cipher, '', $this->mode, '');
    
        $key_len =  phpseclib_mcrypt_enc_get_key_size($td);
        $iv_len =  phpseclib_mcrypt_enc_get_iv_size($td);
    
        $total_len = $key_len + $iv_len;
        $salted = '';
        $dx = '';
        
        // Salt the key and iv
        while (strlen($salted) < $total_len) {
            $dx = md5($dx . $this->key . $salt, true);
            $salted .= $dx;
        }
    
        phpseclib_mcrypt_generic_init($td, substr($salted, 0, $key_len), substr($salted, $key_len, $iv_len));
        $encrypted_data = phpseclib_mcrypt_generic($td, $data);
        phpseclib_mcrypt_generic_deinit($td);
        phpseclib_mcrypt_module_close($td);
    
        //return chunk_split(base64_encode('Salted__' . $salt . $encrypted_data),32,"\r\n");
        return base64_encode('Salted__' . $salt . $encrypted_data);
    }
}