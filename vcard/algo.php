<?php
namespace MatchMovePay\VCard;

use MatchMovePay\Helper\Exception;

class Algo {

    protected $raw = null;
    
    public static function factory($name, $payload, $key) {
        $class = '\\MatchMovePay\\VCard\\Algo\\' . str_replace('-', '', strtoupper($name));
        
        if (!class_exists($class)) {
            throw new Exception('Algo :name is not supported. See `--help`.', [':name' => $name]);
        }
        
        $class = new \ReflectionClass($class);
        return $class->newInstance($payload, $key);
    }
    
    public function __construct($payload) {
        $this->raw = $payload;
    }
    
    public function raw() {
        return $this->raw;
    }
    
    public function encode64() {
        return base64_encode($this->raw);
    }
    
    public function __toString() {
        return $this->encode64();
    }
    
}